$(document).ready(function () {
    $('#tabel_buks').DataTable({
        "ajax": {
            "url": '/jeson/',
        },

        "columns": [
            {data: "volumeInfo.title",}, {data: "volumeInfo.authors[,]",},
            {
                orderable: false,
                data: "accessInfo.webReaderLink",
                render: function (data, type, row, meta) {
                    return "<a style='color: cornflowerblue' href='" + data + "'>Link</a>"
                }

            },

            {
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    return "<span class='fa fa-star'></span>"
                }


            }

        ],

    });


    // Apa yang terjadi jika di klik

    // Digunakan untuk menghitung berapa yang sudah di klik
    var counter = 0;

    // dipanggil jika element dengan class .fa di klik
    $(document).on('click', '.fa', function () {

        // Mengambil elemen dengan class .angkaCounter
        // Lalu mengubahnya sesuai dengan variable counter
        // yang dibuat sebelumnya
        $(".angkaCounter").html(counter);

        // jika classnya masih "fa fa-star" artinya
        // icon tersebut belum di klik

        if (this.className === "fa fa-star") {
            // Akan menambahkan counter
            counter++;
            // Mengubah .angkaCounter dengan counter
            $(".angkaCounter").html(counter);
            $.post('/', {'jml' : counter})
            // mengubah icon dengan class "fa fa-star" menjadi "fa fa-star checked"
            $(this).addClass("checked");
        }
        // jika classnya sudah "fa fa-star checked" artinya
        // Sudah di klik
        else {
            // Akan mengurangi counter
            counter--;
            // Mengubah .angkaCounter dengan counter
            $(".angkaCounter").html(counter);
            $.post('/', {'jml' : counter})
            // mengubah icon dengan class "fa fa-star checked" menjadi "fa fa-star"
            $(this).removeClass("checked");
        }

    });


});